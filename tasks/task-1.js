/* globals $ */

/* 

Create a function that takes a selector and COUNT, then generates inside a UL with COUNT LIs:   
  * The UL must have a class `items-list`
  * Each of the LIs must:
    * have a class `list-item`
    * content "List item #INDEX"
      * The indices are zero-based
  * If the provided selector does not selects anything, do nothing
  * Throws if
    * COUNT is a `Number`, but is less than 1
    * COUNT is **missing**, or **not convertible** to `Number`
      * _Example:_
        * Valid COUNT values:
          * 1, 2, 3, '1', '4', '1123'
        * Invalid COUNT values:
          * '123px' 'John', {}, [] 
          */

          function solve() {
            var Params = {
              ul:{
                class:'items-list'
              },
              li:{
                class:'list-item',
                content:'List item #'
              }
            };
            let validateCount = function (count) {
              if (count<1) {
                throw 'Given count is less than one!!!';
              }
              if (count === 'undefined' || isNaN(+count)) {
                throw 'Parameter count is missing or is not convertable to number!!!';
              }
            }

            let validateSelector = function (selector) {
              if (typeof selector != 'string') {
                throw 'invalid selector!!!';
              }
            }

            function  createList (selector, count) {
              var $ul, $li;
              if ($(selector) == null) { //Do nothing if selector doesn't select anything;
              return;
            }
            $ul = $('<ul />')
            .addClass(Params.ul.class);
            for (var i = 0; i < count; i += 1){
              $li = $('<li />')
              .addClass(Params.li.class)
              .text(Params.li.content + i)
              .appendTo($ul);
            }
            return $($ul).appendTo(selector);
          }

          return function (selector, count) {
            validateCount(count);
            validateSelector(selector);
            createList(selector, count);
          };
        };

        module.exports = solve;
