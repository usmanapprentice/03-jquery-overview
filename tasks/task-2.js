/* globals $ */

/*
Create a function that takes a selector and:
* Finds all elements with class `button` or `content` within the provided element
  * Change the content of all `.button` elements with "hide"
* When a `.button` is clicked:
  * Find the topmost `.content` element, that is before another `.button` and:
    * If the `.content` is visible:
      * Hide the `.content`
      * Change the content of the `.button` to "show"       
    * If the `.content` is hidden:
      * Show the `.content`
      * Change the content of the `.button` to "hide"
    * If there isn't a `.content` element **after the clicked `.button`** and **before other `.button`**, do nothing
* Throws if:
  * The provided ID is not a **jQuery object** or a `string` 

  */
  function solve() {
    function validateSelector (argument) {  
      if (typeof argument == 'undefined') {
        throw 'Params not defined!!!';
      }
      if (argument === ' undefined' || argument == null) {
        throw 'Undefined provided';
      }
      if ($(argument).length < 1) {
        throw 'Null provided';
      }
      return true;
    }
    let btnEvent = function () {
      $(this).text('hide').click(function() {
        var $clicked = $(this),
        $content = $clicked.next('.content');
        if ($content.length && $content.nextAll('.button').length) {
          if ($content.css('display') === 'none') {
            $clicked.text('hide');
            $content.css('display', '');
          }else{
            $clicked.text('show');
            $content.css('display', 'none');
          }
        }
      });
    }
    return function (selector) {
      validateSelector(selector);
      let element = $(selector);
      element.children('.button')
      .each(btnEvent);
    };
  };
  
  module.exports = solve;